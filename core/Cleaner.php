<?php

namespace Core;

class Cleaner {
  
  public function request($field=false) {
    $data = [];
    if($field) {
      $data = static::sanitize($_REQUEST[$field]);
    } else {
      foreach($_REQUEST as $col => $val) {
        $data[$col] = static::sanitize($val);
      }
    }
    return $data;
  }

  public static function sanitize($dirty) {
    $clean = htmlentities(trim($dirty),ENT_QUOTES, "UTF-8");
    return $clean;
  }

}