<?php

namespace Core;

class Validator {

    protected $_model;
    public $field, $error, $value, $rule;
    public $passed = true;

  public function __construct($model, $field, $error, $rule=false){
      $this->_model = $model;
      $this->field = $field;
      $this->error = $error;
      $this->value = $this->_model->{$field};
      $this->rule = $rule;
  }

}