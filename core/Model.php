<?php

namespace Core;
use Core\DB;

class Model {

    public $valid = true;
    public $errors = [];

  public function query($sql, $binds=[]) {
    return DB::getInstance()->query($sql, $binds)->results();
  }

  public function sqlSelect($params) {
    $table = static::$_table;
    $where = (isset($params['where']) ? " WHERE {$params['where']}" : "");
    $order = (isset($params['order']) ? " ORDER BY {$params['order']}" : "");
    $limit = (isset($params['limit']) ? " LIMIT {$params['limit']}" : "");
    $offset = (isset($params['offset']) ? " OFFSET {$params['offset']}" : "");
    $sql = "SELECT * FROM {$table} {$where} {$order} {$limit} {$offset}";
    return $this->query($sql);
  }

  public function sqlInsert($data) {
    $table = static::$_table;
    $columns = [];
    $values = [];
    $binds = [];
    foreach($data as $column => $value) {
      $columns[] = $column;
      $values[] = "?";
      $binds[] = $value;
    }
    $columnString = implode(', ', $columns);
    $valueString = implode(',', $values);
    $sql = "INSERT INTO {$table} ({$columnString}) VALUES ({$valueString})";
    return $this->query($sql, $binds);
  }

  public function getErrors() {
      return $this->errors;
  }

  public function runValidation($validator) {
      $validator->run();
      if(!$validator->passed) {
          $this->valid = false;
          $this->errors[$validator->field] = $validator->error;
      }
      return $validator->passed;
  }

}