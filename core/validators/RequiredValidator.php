<?php

namespace Core\Validators;
use Core\Validator;

class RequiredValidator extends Validator {

    public function run() {
        $this->passed = (!empty($this->value));
    }

}