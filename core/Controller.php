<?php

namespace Core;
use Core\DB;

class Controller {

  protected static $_db;

  public function __construct() {
    self::$_db = DB::getInstance();
  }

  public function jsonResponse($data) {
    // setting headers
    header('Content-Type: application/json');
    // echoing json encodede data for response
    echo json_encode($data);
    exit;
  }

}