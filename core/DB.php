<?php

namespace Core;
use \PDO;

class DB {

  private static $_db = false;
  private static $_pdo = false;
  protected $_success = false;
  protected $_results = [];

  public function __construct() {
    try {
      self::$_pdo = new PDO("mysql:dbname=".DB_NAME.";host=".DB_HOST,DB_USER,DB_PASSWORD);
    } catch(PDOException $e) {
      echo 'Connection failed: '.$e->getMessage();
    }
  }

  public static function getInstance() {
    if(!self::$_db) {
      self::$_db = new self();
    }
    return self::$_db;
  }

  public function query($sql,$binds=[]) {
    $dbh = self::$_pdo->prepare($sql);
    $this->_success = $dbh->execute($binds);
    $this->_results = $dbh->fetchAll(PDO::FETCH_OBJ);
    return $this;
  }

  public function results() {
    return $this->_results;
  }

  public function insert($table, $columns, $values) {
    $columnString = "";
    $valueString = "";
    foreach($columns as $column) {
      $columnString .= $column.", ";
      $valueString .= "?, ";
    }
    $sql = "INSERT INTO {$table} ({rtrim($columnString, ',')}) VALUES ({rtrim($valueString, ',')})";
    return $this->query($sql, $values);
  }

}