import {jsonPost} from '../../helpers/ajax'

export default class Form {

  url;
  data;
  success;

  constructor(url, data, success) {
    this.url = url;
    this.data = data;
    this.success = success;
  }

  submit = () => {
    jsonPost(this.url, this.data, this.success);
  };

  getErrors = (errors, field) => {
    if(errors) {
      if(field in errors) {
        return errors[field];
      } else {
        return "";
      }
    }
  };

}