import React from 'react'
import {observable, action, computed} from 'mobx'
import {observer} from 'mobx-react'
import Form from './Form'

@observer
class AddReviewForm extends React.Component {

  @observable rating;
  @observable errors;
  data = {
    'title': '',
    'rating': '',
    'synopsis': '',
    'review': ''
  };
  url = '/api/admin/addReview';
  success = (resp) => {
    console.log(resp);
    if(resp['success']) {
      window.alert("Review Created Successfully");
    } else {
      this.errors = resp.errors;
    }
  };
  form = new Form(this.url, this.data, this.success);

  @action setRating = (event) => {
    this.rating = event.target.value
    this.data['rating'] = this.rating
  }

  @computed get getRating() {
    let stars = this.getStars()
    return stars.map((star, index) => {
      return star
    })
  }

  getStars = () => {
    let stars = []
    for(let s = 0; s < this.rating; s++) {
      stars.push(<span className="fas fa-star"></span>)
    }
    return stars
  }

  updateData = (event) => {
    this.data[event.target.name] = event.target.value
  }

  addReview = (event) => {
    event.preventDefault()
    this.form.submit()
  }

  render() {
    return(
      <div id="admin-comp">
        <h1 className="subtitle">Post your legacy.</h1>
        <div id="container">
          <form id="add-review-form">
            <h1>Add New Review</h1><br />
            <div className="row">
              <label for="add-review-title">Title {this.getRating}</label>
            </div>
            <div className="row">
              <input type="text" id="add-review-title" name="title" autoComplete="off" onChange={this.updateData} />
              <span>{this.form.getErrors(this.errors, 'title')}</span>
            </div>
            <div className="row">
              <input type="range" id="add-review-rating" name="rating" defaultValue="0" min="0" max="5" onChange={this.setRating} />
              <label for="add-review-rating">Rating</label>
            </div>
            <div className="row">
              <label for="add-review-synopsis">Synopsis</label>
            </div>
            <div className="row">
              <input type="text" id="add-review-synopsis" name="synopsis" autoComplete="off" onChange={this.updateData} />
            </div>
            <div className="row">
              <label for="add-review-full">Full Review</label>
            </div>
            <div className="row">
              <input type="text" id="add-review-full" name="review" autoComplete="off" onChange={this.updateData} />
            </div>
            <div className="row">
              <button id="add-review-submit" onClick={this.addReview}>Post</button>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

export default AddReviewForm