import React from 'react'
import {observer} from 'mobx-react'
import {observable, computed, action} from 'mobx'
import {jsonPost} from '../helpers/ajax'
import {Link} from 'react-router-dom'
import ReviewContainer from './ReviewContainer'

@observer
class Reviews extends React.Component {

  @observable reviews = []
  @observable filter = "newest"
  searchRequest = ""

  updateSearchRequest = (event) => {
    this.searchRequest = event.target.value
    this.getReviews()
  }

  showFilter = () => {
    return "Filter By: " + this.filter
  }

  @action changeFilter = (event) => {
    this.filter = event.target.id
    this.getReviews()
  }

  @action getReviews = () => {
    jsonPost("/api/reviews/get", {search:this.searchRequest, filter:this.filter}, (resp)=>{
      this.reviews = resp.data
    })
  }

  @computed get showReviews() {
    return this.reviews.map((review,index)=>{
      return <ReviewContainer key={index} title={review.title} rating={review.rating} synopsis={review.synopsis} content={review.content} />
    })
  }

  componentDidMount() {
    this.getReviews()
  }

  render() {
    return(
      <div id="rev-comp">
        <h1 className="subtitle">Browse our catalog of over 1, 500 reviews.</h1>
        <div id="container">
          <Link to="/admin"><h4><span className="fas fa-cog"></span> admin</h4></Link>
          <div className="filters">
            <span onClick={this.changeFilter} id="newest"> newest -></span><br />
            <span onClick={this.changeFilter} id="rating"> rating -></span><br />
            <span onClick={this.changeFilter} id="title"> title -></span>
          </div>
          <section className="content">
            <div className="controls">
              <input type="text" disabled value={this.showFilter()}/>
              <input name="searchbar" type="text" placeholder="Search Reviews..." onChange={this.updateSearchRequest} autoComplete="off"/>
            </div>
            <div className="reviews">{this.showReviews}</div>
          </section>
        </div>
      </div>
    )
  }
}

export default Reviews