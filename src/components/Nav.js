import React from 'react'
import {Link} from 'react-router-dom'

export default class Nav extends React.Component {
  render() {
    return(
      <div id="nav-comp">
        <nav>
          <ul>
            <li><Link to="/"><span className="fas fa-igloo"></span> Home</Link></li>
            <li><Link to="/shop"><span className="fas fa-store"></span> Shop</Link></li>
            <li><Link to="/reviews"><span className="fas fa-binoculars"></span> Reviews</Link></li>
            <li><Link to="/videos"><span className="fas fa-file-video"></span> Videos</Link></li>
            <li><Link to="/about"><span className="fas fa-user-secret"></span> About</Link></li>
          </ul>
        </nav>
      </div>
    )
  }
}