import React from 'react'
import {Link} from 'react-router-dom'
import {observer} from 'mobx-react'
import {observable} from 'mobx'

@observer
class ReviewContainer extends React.Component {

  @observable path = ""

  renderRating = () => {
    let rating = []
    for(let r = 0; r < this.props.rating; r++) {
      rating.push(<span className='fas fa-star' key={r}></span>)
    }
    return rating
  }

  componentDidMount() {
    this.path = "/read/"+this.props.title.toLowerCase().replace(/ /g, '_')
  }

  render() {
    return(
      <Link to={this.path}>
        <div className="review">
          <h1>{this.props.title}</h1>
          <h3>{this.props.synopsis}</h3>
          <span>{this.renderRating()}</span>
        </div>
        <br />
      </Link>
    )
  }
}

export default ReviewContainer