import React from 'react'
import AddReviewForm from './forms/AddReviewForm'

export default class Admin extends React.Component {
  render() {
    return(
      <div>
        <AddReviewForm />
        {/* other forms can go here */}
      </div>
    )
  }
}