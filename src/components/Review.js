import React from 'react'
import {observer} from 'mobx-react'
import {observable} from 'mobx'
import {jsonPost} from '../helpers/ajax'

@observer
class Review extends React.Component {

  @observable title
  @observable rating
  @observable review

  success = (resp) => {
    console.log(resp)
    this.rating = resp.result[0].rating
    this.review = resp.result[0].review
  }

  componentDidMount() {
    this.title = this.props.match.params.game.replace(/_/g, ' ')
    jsonPost('/api/reviews/read', {title:this.title}, this.success)
  }

  render() {
    return(
      <div id="read-comp">
        <h1 className="subtitle">{this.title}</h1>
        <h1 className="subtitle" id="rating">{this.rating} / 5 Stars</h1>
        <p>{this.review}</p>
      </div>
    )
  }
}

export default Review