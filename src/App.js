import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import Nav from './components/Nav'
import Home from './components/Home'
import Shop from './components/Shop'
import Reviews from './components/Reviews'
import Videos from './components/Videos'
import About from './components/About'
import Admin from './components/Admin'
import Review from './components/Review'

export default class App extends React.Component{
  render() {
    return (
      <BrowserRouter>
        <div>
          <Nav />
          <Route exact path="/" component={Home}/>
          <Route exact path="/shop" component={Shop}/>
          <Route exact path="/reviews" component={Reviews}/>
          <Route exact path="/videos" component={Videos}/>
          <Route exact path="/about" component={About}/>
          <Route exact path="/admin" component={Admin}/>
          <Route path="/read/:game" component={Review}/>
        </div>
      </BrowserRouter>
    )
  }
}