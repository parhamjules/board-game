var qs = require('qs');

export function jsonPost(url,data,onSuccess,stringify){
  const postBody = parseDataForPost(data,stringify);
  return fetch(url,{method: 'POST', credentials: 'same-origin', body:postBody,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
      }
    }).then((response)=>{
        return response.json();
    }).then((resp)=>{
      if (typeof onSuccess === "undefined" || onSuccess === null) {
        return resp;
      } else {
        return onSuccess(resp);
      }
    });
}

export function parseDataForPost(data,stringify){
  if(typeof stringify === 'undefined'){
    return qs.stringify(data,{arrayFormat: 'brackets'});
  }
  return JSON.stringify(data);
}