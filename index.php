<?php

require('./api/config.php');

define('DS',DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));

session_start();

function autoload($className){
  $classAry = explode('\\',$className);
  $class = array_pop($classAry);
  $subPath = strtolower(implode(DS,$classAry));
  $path = ROOT . DS . $subPath . DS . $class . '.php';
  if(file_exists($path)){
    require_once($path);
  }
}
spl_autoload_register('autoload');

// getting full path
$path_info = (isset($_SERVER['PATH_INFO']))? ltrim($_SERVER['PATH_INFO'],'/') : '/';
$url = explode('/',$path_info);
if($url[0] != 'api'){
  include_once('build/index.html');
  exit;
}
// getting controller
array_shift($url);
$controller = 'Api\Controllers\\'.ucwords($url[0]). "Controller";
// getting method
array_shift($url);
$method = (!empty($url[0]))? $url[0] : 'index';
$method = $method . 'Action';
// getting other path parameters
array_shift($url);
$queryParams = $url;
// calling method
$dispatch = new $controller();
if(method_exists($controller, $method)) {
  call_user_func_array([$dispatch, $method], $queryParams);
} else {
  die('That method does not exist in the controller \"' . $controller . '\"');
}