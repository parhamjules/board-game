<?php

namespace Api\Controllers;
use Core\{Controller,Cleaner};
use Api\Models\Reviews;

class AdminController extends Controller {

  public function addReviewAction() {
    $data = Cleaner::request();
    $model = new Reviews();
    $results = $model->addReview($data);
    $errors = $model->getErrors();
    $success = empty($errors);
    $resp = [
      'success'=>$success,
      'data'=>$results,
      'errors'=>$errors
    ];
    $this->jsonResponse($resp);
  }

}