<?php

namespace Api\Controllers;
use Core\{Controller,Cleaner};
use Api\Models\Reviews;

class ReviewsController extends Controller {

  public function getAction() {
    $search = Cleaner::request('search');
    $filter = Cleaner::request('filter');
    $order = $this->getOrderFromFilter($filter);
    $model = new Reviews();
    $results = $model->sqlSelect([
      'order'=>$order
    ]);
    $reviews = [];
    if(strlen($search) > 0) {
      foreach($results as $index => $result) {
        if(strtolower(substr($result->title, 0, strlen($search))) == trim(strtolower($search))) {
          $reviews[] = $result;
        }
      }
    } else {
      $reviews = $results;
    }
    $resp = [
      'success'=>true,
      'data'=>$reviews
    ];
    $this->jsonResponse($resp);
  }

  public function getOrderFromFilter($filter) {
    switch($filter) {
      case 'newest':
        return 'id DESC';
      case 'rating':
        return 'rating DESC';
      case 'title':
        return 'title';
    }
  }

  public function readAction() {
    $lowerTitle = Cleaner::request('title');
    $titleArray = [];
    foreach(explode(' ', $lowerTitle) as $word) {
      $titleArray[] = ucfirst($word);
    }
    $title = implode(' ', $titleArray);
    $model = new Reviews();
    $result = $model->sqlSelect([
      'where'=>"title = '{$title}'"
    ]);
    $resp = [
      'success'=>true,
      'result'=>$result
    ];
    $this->jsonResponse($resp);
  }

}