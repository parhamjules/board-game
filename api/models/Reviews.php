<?php

namespace Api\Models;
use Core\Model;
use Core\Validators\RequiredValidator;

class Reviews extends Model{

  protected static $_table = 'reviews';
  public $title, $rating, $synopsis, $review;

  public function addReview($data) {
      $this->title = $data['title'];
      $this->rating = $data['rating'];
      $this->synopsis = $data['synopsis'];
      $this->review = $data['review'];
      if($this->validator()) {
          $this->sqlInsert($data);
      }
  }

  public function validator() {
      $titleValidator = new RequiredValidator($this, 'title', 'You must put a game title.');
      $this->runValidation($titleValidator);
  }

}